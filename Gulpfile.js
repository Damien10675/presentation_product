//Dependances
//--------------------------------------
var gulp = require('gulp');
var sass = require('gulp-sass');


//Tasks
//--------------------------------------

gulp.task('styles', function() {
    gulp.src('Design/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./Design/css/'))
});

gulp.task('watch',function() {
    gulp.watch('Design/scss/**/*.scss',['styles']);
});